# wjd-logo
`(c) Tobias Hocke 2021-2024`
## Logo Creator für die Wirtschaftsjunioren

Um das Corporate-Design der Wirtschaftsjunioren Deutschland e.V. Verbandsübergreifend nutzbar zu machen bieten wir einen Logo Creator an. Das Logo wird als Vektordatei (SVG) generiert. Dieser kann wiefolgt genutzt werden:

### Basis-Nutzung
Um das Logo zu generieren kann einfach die URL kopiert und entsprechend angepasst werden. Der **Kreisname** wird entsprechend gerendert.

https://logo.wjd.de/Kreisname?color=blue (CD-Farben auf transparent)

![Kreisname](https://logo.wjd.de/Kreisname)

https://logo.wjd.de/Kreisname?color=white (weiß auf transparent (sieht im Browser "weiß" aus, muss auf farbigen Hintergrund sein, bspw. Webseite))

![Kreisname](https://logo.wjd.de/Kreisname?color=white)

### Lange Namen
Gemäß Corporate-Design Richtlinie ist der Kreisname umzubrechen, sofern er eine längere Laufweite hat als "WIRTSCHAFTSJUNIOREN". Dies kann mittels eines **_** erfolgen.

https://logo.wjd.de/Ich%20habe%20einen-_langen-Kreisnamen (CD-Farben auf transparent)

![Kreisname](https://logo.wjd.de/Ich%20habe%20einen-_langen-Kreisnamen)

https://logo.wjd.de/Ich%20habe%20einen-_langen-Kreisnamen?color=white (weiß auf transparent (sieht im Browser "weiß" aus, muss auf farbigen Hintergrund sein, bspw. Webseite))

![Kreisname](https://logo.wjd.de/Ich%20habe%20einen-_langen-Kreisnamen?color=white)

### JCI
Auch das Erzeugen eines JCI-Logos ist möglich! Hierzu muss der Parameter `logo=jci` ergänzt werden. 

https://logo.wjd.de/Germany?color=blue&logo=jci

![Germany](https://logo.wjd.de/Germany?logo=jci)

## Assets
Das Asset *Linotype - HelveticaNeueLTStd-Bd.otf* ist nicht im Repository, da es lizensiert werden muss: https://www.myfonts.com/fonts/linotype/neue-helvetica/pro-75-bold-189171/

## Installation
- Inhalt des Repository in ZIP-Archiv bündeln und als Lambda Funktion mit NodeJS v14.x hochladen
- Application Load Balancer auf Lambda-Funktion verweisen

## Changelog

### 2024-04 (1.3.0)
- Logo-Option `27prozentvonuns` im Rahmen der DIHK Kampagne https://27prozentvonuns.de/ #27prozentvonuns (Farboptionen color und white)

### 2023-01 (1.2.0)
- Umstellung auf AWS SAM

### 2022-05 (1.1.0)
- Logo-Option `diversity` im Rahmen des https://www.charta-der-vielfalt.de/aktivitaeten/deutscher-diversity-tag/ #DDT22
- JCI Logo: Organisationsname in JCI Gold aus CD Änderung und JCI Blau aus CD